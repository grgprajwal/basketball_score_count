package com.grgprajwal26.basketballscorecount;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {
    public static final String Ascore ="sA";
    public static final String Bscore ="sB";
    private static int sA=0,sB=0;
    TextView scoreA, scoreB;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Basketball Score");

        Button a1,a2,a3,b1,b2,b3;

        scoreA = (TextView)findViewById(R.id.groupAscore);
        scoreB = (TextView) findViewById(R.id.groupBscore);

        a1 = (Button)findViewById(R.id.groupA1point);
        a2 = (Button)findViewById(R.id.groupA2point);
        a3 = (Button)findViewById(R.id.groupA3point);
        b1 = (Button)findViewById(R.id.groupB1point);
        b2 = (Button)findViewById(R.id.groupB2point);
        b3 = (Button)findViewById(R.id.groupB3point);

        a1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOnePoint(1);
            }
        });

        a2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTwoPoint(1);
            }
        });
        a3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onThreePoint(1);
            }
        });


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOnePoint(2);
            }
        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTwoPoint(2);
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onThreePoint(2);
            }
        });

    }

    @Override
    protected void onDestroy() {
        onReset();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // putSeraliazable if object are neededto be put or enum data are to be send
        // values are save just before orientation changes.
        outState.putInt(Ascore,sA);
        outState.putInt(Bscore,sB);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        sA = savedInstanceState.getInt(Ascore);
        sB=savedInstanceState.getInt(Bscore);
        scoreA.setText(Integer.toString(sA));
        scoreB.setText(Integer.toString(sB));
    }


    public void onOnePoint(int flag){
        if(flag ==1){
            sA++;
            scoreA.setText(Integer.toString(sA));
        }
        else{
            sB++;
            scoreB.setText(Integer.toString(sB));
        }


    }

    public void onTwoPoint(int flag){
        if(flag ==1){
            sA=sA+2;
            scoreA.setText(Integer.toString(sA));
        }
        else{
            sB=sB+2;
            scoreB.setText(Integer.toString(sB));
        }


    }

    public void onThreePoint(int flag){
        if(flag ==1){
            sA=sA+3;
            scoreA.setText(Integer.toString(sA));
        }
        else{
            sB=sB+3;
            scoreB.setText(Integer.toString(sB));
        }


    }

    public void onReset(){
        sA=0;
        sB=0;
        scoreA.setText(Integer.toString(sA));
        scoreB.setText(Integer.toString(sB));


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== R.id.resetScore){
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setCancelable(false);
            alert.setMessage("Do you want to reset the score?");
            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onReset();
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });


            AlertDialog builder = alert.create();
            builder.setTitle("Reset");
            builder.show();

            return true;

        }
        if(item.getItemId()==R.id.exit){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Do you want to exit?");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onReset();
                    System.exit(0);
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            AlertDialog alert = builder.create();
            alert.setTitle("Exit");
            alert.show();

            return true;
        }
        return true;
    }
}




